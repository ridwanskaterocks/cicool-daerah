<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Data Controller
*| --------------------------------------------------------------------------
*| Form Data site
*|
*/
class Form_data extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_data');
		$this->load->model('Daerah_model', 'daerah');
	}

	/**
	* show all Form Datas
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('form_data_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['form_datas'] = $this->model_form_data->get($filter, $field, $this->limit_page, $offset);
		$this->data['form_data_counts'] = $this->model_form_data->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/form_data/index/',
			'total_rows'   => $this->model_form_data->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Data List');
		$this->render('backend/standart/administrator/form_builder/form_data/form_data_list', $this->data);
	}

	/**
	* Update view Form Datas
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('form_data_update');

		$this->data['form_data'] = $this->model_form_data->find($id);

		$this->template->title('Data Update');
		$this->render('backend/standart/administrator/form_builder/form_data/form_data_update', $this->data);
	}

	/**
	* Update Form Datas
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('form_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
		$this->form_validation->set_rules('kota_kabupaten', 'Kota / Kabupaten', 'trim|required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required');
		$this->form_validation->set_rules('kelurahan_desa', 'Kelurahan / Desa', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('form_data_file_1_name', 'File 1', 'trim|required');
		$this->form_validation->set_rules('form_data_file_2_name', 'File 2', 'trim|required');
		$this->form_validation->set_rules('form_data_file_3_name', 'File 3', 'trim|required');
		$this->form_validation->set_rules('form_data_file_4_name', 'File 4', 'trim|required');
		$this->form_validation->set_rules('form_data_file_5_name', 'File 5', 'trim|required');
		
		if ($this->form_validation->run()) {
			$form_data_file_1_uuid = $this->input->post('form_data_file_1_uuid');
			$form_data_file_1_name = $this->input->post('form_data_file_1_name');
			$form_data_file_2_uuid = $this->input->post('form_data_file_2_uuid');
			$form_data_file_2_name = $this->input->post('form_data_file_2_name');
			$form_data_file_3_uuid = $this->input->post('form_data_file_3_uuid');
			$form_data_file_3_name = $this->input->post('form_data_file_3_name');
			$form_data_file_4_uuid = $this->input->post('form_data_file_4_uuid');
			$form_data_file_4_name = $this->input->post('form_data_file_4_name');
			$form_data_file_5_uuid = $this->input->post('form_data_file_5_uuid');
			$form_data_file_5_name = $this->input->post('form_data_file_5_name');
		
			$save_data = [
				'provinsi' => $this->input->post('provinsi'),
				'kota_kabupaten' => $this->input->post('kota_kabupaten'),
				'kecamatan' => $this->input->post('kecamatan'),
				'kelurahan_desa' => $this->input->post('kelurahan_desa'),
				'nama' => $this->input->post('nama'),
				'nik' => $this->input->post('nik'),
				'email' => $this->input->post('email'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'tahun_kegiatan' => $this->input->post('tahun_kegiatan'),
			];

			if (!is_dir(FCPATH . '/uploads/form_data/')) {
				mkdir(FCPATH . '/uploads/form_data/');
			}

			if (!empty($form_data_file_1_uuid)) {
				$form_data_file_1_name_copy = date('YmdHis') . '-' . $form_data_file_1_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_1_uuid . '/' . $form_data_file_1_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_1_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_1'] = $form_data_file_1_name_copy;
			}
		
			if (!empty($form_data_file_2_uuid)) {
				$form_data_file_2_name_copy = date('YmdHis') . '-' . $form_data_file_2_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_2_uuid . '/' . $form_data_file_2_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_2_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_2_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_2'] = $form_data_file_2_name_copy;
			}
		
			if (!empty($form_data_file_3_uuid)) {
				$form_data_file_3_name_copy = date('YmdHis') . '-' . $form_data_file_3_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_3_uuid . '/' . $form_data_file_3_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_3_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_3_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_3'] = $form_data_file_3_name_copy;
			}
		
			if (!empty($form_data_file_4_uuid)) {
				$form_data_file_4_name_copy = date('YmdHis') . '-' . $form_data_file_4_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_4_uuid . '/' . $form_data_file_4_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_4_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_4_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_4'] = $form_data_file_4_name_copy;
			}
		
			if (!empty($form_data_file_5_uuid)) {
				$form_data_file_5_name_copy = date('YmdHis') . '-' . $form_data_file_5_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_5_uuid . '/' . $form_data_file_5_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_5_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_5_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_5'] = $form_data_file_5_name_copy;
			}
		
			
			$save_form_data = $this->model_form_data->change($id, $save_data);

			if ($save_form_data) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/form_data', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/form_data');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					set_message('Your data not change.', 'error');
					
            		$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/form_data');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	/**
	* delete Form Datas
	*
	* @var $id String
	*/
	public function delete($id)
	{
		$this->is_allowed('form_data_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'Form Data'), 'success');
        } else {
            set_message(cclang('error_delete', 'Form Data'), 'error');
        }

		redirect_back();
	}

	/**
	* View view Form Datas
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('form_data_view');

		$this->data['form_data'] = $this->model_form_data->find($id);

		$this->template->title('Data Detail');
		$this->render('backend/standart/administrator/form_builder/form_data/form_data_view', $this->data);
	}

	/**
	* delete Form Datas
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$form_data = $this->model_form_data->find($id);

		if (!empty($form_data->file_1)) {
			$path = FCPATH . '/uploads/form_data/' . $form_data->file_1;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($form_data->file_2)) {
			$path = FCPATH . '/uploads/form_data/' . $form_data->file_2;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($form_data->file_3)) {
			$path = FCPATH . '/uploads/form_data/' . $form_data->file_3;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($form_data->file_4)) {
			$path = FCPATH . '/uploads/form_data/' . $form_data->file_4;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		if (!empty($form_data->file_5)) {
			$path = FCPATH . '/uploads/form_data/' . $form_data->file_5;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}

		
		return $this->model_form_data->remove($id);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_1_file()
	{
		if (!$this->is_allowed('form_data_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_1_file($uuid)
	{
		if (!$this->is_allowed('form_data_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_1', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_1_file($id)
	{
		if (!$this->is_allowed('form_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_1', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_1_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_2_file()
	{
		if (!$this->is_allowed('form_data_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_2_file($uuid)
	{
		if (!$this->is_allowed('form_data_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_2', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_2_file($id)
	{
		if (!$this->is_allowed('form_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_2', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_2_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_3_file()
	{
		if (!$this->is_allowed('form_data_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_3_file($uuid)
	{
		if (!$this->is_allowed('form_data_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_3', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_3_file($id)
	{
		if (!$this->is_allowed('form_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_3', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_3_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_4_file()
	{
		if (!$this->is_allowed('form_data_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_4_file($uuid)
	{
		if (!$this->is_allowed('form_data_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_4', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_4_file($id)
	{
		if (!$this->is_allowed('form_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_4', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_4_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_5_file()
	{
		if (!$this->is_allowed('form_data_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_5_file($uuid)
	{
		if (!$this->is_allowed('form_data_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_5', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_5_file($id)
	{
		if (!$this->is_allowed('form_data_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_5', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_5_file'
        ]);
	}
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('form_data_export');

		$this->model_form_data->export('form_data', 'form_data');
	}
}


/* End of file form_data.php */
/* Location: ./application/controllers/administrator/Form Data.php */