<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Book Controller
*| --------------------------------------------------------------------------
*| Book site
*|
*/
class Book extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_book');
	}

	/**
	* show all Books
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('book_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['books'] = $this->model_book->get($filter, $field, $this->limit_page, $offset);
		$this->data['book_counts'] = $this->model_book->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/book/index/',
			'total_rows'   => $this->model_book->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Book List');
		$this->render('backend/standart/administrator/book/book_list', $this->data);
	}
	
	/**
	* Add new books
	*
	*/
	public function add()
	{
		$this->is_allowed('book_add');

		$this->template->title('Book New');
		$this->render('backend/standart/administrator/book/book_add', $this->data);
	}

	/**
	* Add New Books
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('book_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('author', 'Author', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$this->form_validation->set_rules('book_image_name', 'Image', 'trim|required');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price[]', 'Price', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
			$book_image_uuid = $this->input->post('book_image_uuid');
			$book_image_name = $this->input->post('book_image_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'author' => $this->input->post('author'),
				'description' => $this->input->post('description'),
				'stock' => $this->input->post('stock'),
				'price' => implode(',', (array) $this->input->post('price')),
				'publish_date' => $this->input->post('publish_date'),
			];

			if (!is_dir(FCPATH . '/uploads/book/')) {
				mkdir(FCPATH . '/uploads/book/');
			}

			if (!empty($book_image_name)) {
				$book_image_name_copy = date('YmdHis') . '-' . $book_image_name;

				rename(FCPATH . 'uploads/tmp/' . $book_image_uuid . '/' . $book_image_name, 
						FCPATH . 'uploads/book/' . $book_image_name_copy);

				if (!is_file(FCPATH . '/uploads/book/' . $book_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['image'] = $book_image_name_copy;
			}
		
			
			$save_book = $this->model_book->store($save_data);

			if ($save_book) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_book;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/book/edit/' . $save_book, 'Edit Book'),
						anchor('administrator/book', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/book/edit/' . $save_book, 'Edit Book')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/book');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/book');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Books
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('book_update');

		$this->data['book'] = $this->model_book->find($id);

		$this->template->title('Book Update');
		$this->render('backend/standart/administrator/book/book_update', $this->data);
	}

	/**
	* Update Books
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('book_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('author', 'Author', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$this->form_validation->set_rules('book_image_name', 'Image', 'trim|required');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price[]', 'Price', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
			$book_image_uuid = $this->input->post('book_image_uuid');
			$book_image_name = $this->input->post('book_image_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'author' => $this->input->post('author'),
				'description' => $this->input->post('description'),
				'stock' => $this->input->post('stock'),
				'price' => implode(',', (array) $this->input->post('price')),
				'publish_date' => $this->input->post('publish_date'),
			];

			if (!is_dir(FCPATH . '/uploads/book/')) {
				mkdir(FCPATH . '/uploads/book/');
			}

			if (!empty($book_image_uuid)) {
				$book_image_name_copy = date('YmdHis') . '-' . $book_image_name;

				rename(FCPATH . 'uploads/tmp/' . $book_image_uuid . '/' . $book_image_name, 
						FCPATH . 'uploads/book/' . $book_image_name_copy);

				if (!is_file(FCPATH . '/uploads/book/' . $book_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['image'] = $book_image_name_copy;
			}
		
			
			$save_book = $this->model_book->change($id, $save_data);

			if ($save_book) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/book', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/book');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/book');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Books
	*
	* @var $id String
	*/
	public function delete($id)
	{
		$this->is_allowed('book_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'book'), 'success');
        } else {
            set_message(cclang('error_delete', 'book'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Books
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('book_view');

		$this->data['book'] = $this->model_book->join_avaiable()->find($id);

		$this->template->title('Book Detail');
		$this->render('backend/standart/administrator/book/book_view', $this->data);
	}
	
	/**
	* delete Books
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$book = $this->model_book->find($id);

		if (!empty($book->image)) {
			$path = FCPATH . '/uploads/book/' . $book->image;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_book->remove($id);
	}
	
	/**
	* Upload Image Book	* 
	* @return JSON
	*/
	public function upload_image_file()
	{
		if (!$this->is_allowed('book_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'book',
		]);
	}

	/**
	* Delete Image Book	* 
	* @return JSON
	*/
	public function delete_image_file($uuid)
	{
		if (!$this->is_allowed('book_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'image', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'book',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/book/'
        ]);
	}

	/**
	* Get Image Book	* 
	* @return JSON
	*/
	public function get_image_file($id)
	{
		if (!$this->is_allowed('book_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$book = $this->model_book->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'image', 
            'table_name'        => 'book',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/book/',
            'delete_endpoint'   => 'administrator/book/delete_image_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('book_export');

		$this->model_book->export('book', 'book');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('book_export');

		$this->model_book->pdf('book', 'book');
	}
}


/* End of file book.php */
/* Location: ./application/controllers/administrator/Book.php */