<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Data Controller
*| --------------------------------------------------------------------------
*| Form Data site
*|
*/
class Form_data extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_data');
	}

	/**
	* Submit Form Datas
	*
	*/
	public function submit()
	{
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
		$this->form_validation->set_rules('kota_kabupaten', 'Kota / Kabupaten', 'trim|required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required');
		$this->form_validation->set_rules('kelurahan_desa', 'Kelurahan / Desa', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('form_data_file_1_name', 'File 1', 'trim|required');
		$this->form_validation->set_rules('form_data_file_2_name', 'File 2', 'trim|required');
		$this->form_validation->set_rules('form_data_file_3_name', 'File 3', 'trim|required');
		$this->form_validation->set_rules('form_data_file_4_name', 'File 4', 'trim|required');
		$this->form_validation->set_rules('form_data_file_5_name', 'File 5', 'trim|required');
		
		if ($this->form_validation->run()) {
			$form_data_file_1_uuid = $this->input->post('form_data_file_1_uuid');
			$form_data_file_1_name = $this->input->post('form_data_file_1_name');
			$form_data_file_2_uuid = $this->input->post('form_data_file_2_uuid');
			$form_data_file_2_name = $this->input->post('form_data_file_2_name');
			$form_data_file_3_uuid = $this->input->post('form_data_file_3_uuid');
			$form_data_file_3_name = $this->input->post('form_data_file_3_name');
			$form_data_file_4_uuid = $this->input->post('form_data_file_4_uuid');
			$form_data_file_4_name = $this->input->post('form_data_file_4_name');
			$form_data_file_5_uuid = $this->input->post('form_data_file_5_uuid');
			$form_data_file_5_name = $this->input->post('form_data_file_5_name');
		
			$save_data = [
				'provinsi' => $this->input->post('provinsi'),
				'kota_kabupaten' => $this->input->post('kota_kabupaten'),
				'kecamatan' => $this->input->post('kecamatan'),
				'kelurahan_desa' => $this->input->post('kelurahan_desa'),
				'nama' => $this->input->post('nama'),
				'nik' => $this->input->post('nik'),
				'email' => $this->input->post('email'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'tahun_kegiatan' => $this->input->post('tahun_kegiatan'),
				'file_1' => $this->input->post('file_1'),
				'file_2' => $this->input->post('file_2'),
				'file_3' => $this->input->post('file_3'),
				'file_4' => $this->input->post('file_4'),
				'file_5' => $this->input->post('file_5'),
			];

			if (!is_dir(FCPATH . '/uploads/form_data/')) {
				mkdir(FCPATH . '/uploads/form_data/');
			}

			if (!empty($form_data_file_1_uuid)) {
				$form_data_file_1_name_copy = date('YmdHis') . '-' . $form_data_file_1_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_1_uuid . '/' . $form_data_file_1_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_1_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_1_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_1'] = $form_data_file_1_name_copy;
			}
		
			if (!empty($form_data_file_2_uuid)) {
				$form_data_file_2_name_copy = date('YmdHis') . '-' . $form_data_file_2_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_2_uuid . '/' . $form_data_file_2_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_2_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_2_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_2'] = $form_data_file_2_name_copy;
			}
		
			if (!empty($form_data_file_3_uuid)) {
				$form_data_file_3_name_copy = date('YmdHis') . '-' . $form_data_file_3_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_3_uuid . '/' . $form_data_file_3_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_3_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_3_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_3'] = $form_data_file_3_name_copy;
			}
		
			if (!empty($form_data_file_4_uuid)) {
				$form_data_file_4_name_copy = date('YmdHis') . '-' . $form_data_file_4_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_4_uuid . '/' . $form_data_file_4_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_4_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_4_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_4'] = $form_data_file_4_name_copy;
			}
		
			if (!empty($form_data_file_5_uuid)) {
				$form_data_file_5_name_copy = date('YmdHis') . '-' . $form_data_file_5_name;

				rename(FCPATH . 'uploads/tmp/' . $form_data_file_5_uuid . '/' . $form_data_file_5_name, 
						FCPATH . 'uploads/form_data/' . $form_data_file_5_name_copy);

				if (!is_file(FCPATH . '/uploads/form_data/' . $form_data_file_5_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['file_5'] = $form_data_file_5_name_copy;
			}
		
			
			$save_form_data = $this->model_form_data->store($save_data);

			$this->data['success'] = true;
			$this->data['id'] 	   = $save_form_data;
			$this->data['message'] = cclang('your_data_has_been_successfully_submitted');
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_1_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_1_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_1', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_1_file($id)
	{
		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_1', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_1_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_2_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_2_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_2', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_2_file($id)
	{
		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_2', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_2_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_3_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_3_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_3', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_3_file($id)
	{
		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_3', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_3_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_4_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_4_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_4', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_4_file($id)
	{
		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_4', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_4_file'
        ]);
	}
	
	/**
	* Upload Image Form Data	* 
	* @return JSON
	*/
	public function upload_file_5_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'form_data',
			'allowed_types' => 'jpg|jpeg|png|pdf',
		]);
	}

	/**
	* Delete Image Form Data	* 
	* @return JSON
	*/
	public function delete_file_5_file($uuid)
	{
		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_5', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/'
        ]);
	}

	/**
	* Get Image Form Data	* 
	* @return JSON
	*/
	public function get_file_5_file($id)
	{
		$form_data = $this->model_form_data->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_5', 
            'table_name'        => 'form_data',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/form_data/',
            'delete_endpoint'   => 'administrator/form_data/delete_file_5_file'
        ]);
	}
	
}


/* End of file form_data.php */
/* Location: ./application/controllers/administrator/Form Data.php */