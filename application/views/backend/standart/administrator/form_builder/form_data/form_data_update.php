    <script type="text/javascript" src="<?= BASE_ASSET; ?>js/ajax_daerah.js"></script>

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Data        <small><?= cclang('update'); ?> Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/form_data'); ?>">Data</a></li>
        <li class="active"><?= cclang('update'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Data</h3>
                            <h5 class="widget-user-desc">Edit Data</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/form_data/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_form_data', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_form_data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                        <div class="form-group ">
                            <label for="provinsi" class="col-sm-2 control-label">Provinsi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control" name="provinsi" id="provinsi" data-placeholder="Select Provinsi"  onchange="ajaxkota(this.value)" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('provinsi') as $row): ?>
                                    <option <?=  $row->id_prov ==  $form_data->provinsi ? 'selected' : ''; ?> value="<?= $row->id_prov ?>"><?= $row->nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                        <div class="form-group " id="kab_box">
                            <label for="kota_kabupaten" class="col-sm-2 control-label">Kota / Kabupaten 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control" name="kota_kabupaten" id="kota" data-placeholder="Select Kota / Kabupaten"  onchange="ajaxkec(this.value)" >
                                    <option value=""></option>
                                    <?php foreach ($this->daerah->getKab($form_data->provinsi) as $row): ?>
                                    <option <?=  $row->id_kab ==  $form_data->kota_kabupaten ? 'selected' : ''; ?> value="<?= $row->id_kab ?>"><?= $row->nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                        <div class="form-group " id="kec_box">
                            <label for="kecamatan" class="col-sm-2 control-label">Kecamatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control" name="kecamatan" id="kec" data-placeholder="Select Kecamatan"  onchange="ajaxkel(this.value)" >
                                    <option value=""></option>
                                    <?php foreach ($this->daerah->getKec($form_data->kota_kabupaten) as $row): ?>
                                    <option <?=  $row->id_kec ==  $form_data->kecamatan ? 'selected' : ''; ?> value="<?= $row->id_kec ?>"><?= $row->nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                        <div class="form-group " id="kel_box">
                            <label for="kelurahan_desa" class="col-sm-2 control-label">Kelurahan / Desa 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control" name="kelurahan_desa" id="kel" data-placeholder="Select Kelurahan / Desa"  >
                                    <option value=""></option>
                                    <?php foreach ($this->daerah->getKel($form_data->kecamatan) as $row): ?>
                                    <option <?=  $row->id_kel ==  $form_data->kelurahan_desa ? 'selected' : ''; ?> value="<?= $row->id_kel ?>"><?= $row->nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                        <div class="form-group ">
                            <label for="nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama" value="<?= set_value('nama', $form_data->nama); ?>" id="nama" placeholder=""  >
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="nik" class="col-sm-2 control-label">NIK 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nik" value="<?= set_value('nik', $form_data->nik); ?>" id="nik" placeholder=""  >
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="email" value="<?= set_value('email', $form_data->email); ?>" id="email" placeholder=""  >
                                <small class="info help-block">
                                <b>Format Email must</b> Valid Email.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="jenis_kelamin" id="jenis_kelamin" data-placeholder="Select Jenis Kelamin" >
                                    <option value=""></option>
                                    <option <?= $form_data->jenis_kelamin == "laki_laki" ? 'selected' :''; ?> value="laki_laki">Laki - Laki</option>
                                    <option <?= $form_data->jenis_kelamin == "perempuan" ? 'selected' :''; ?> value="perempuan">Perempuan</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="tahun_kegiatan" class="col-sm-2 control-label">Tahun Kegiatan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="tahun_kegiatan" id="tahun_kegiatan" data-placeholder="Select Tahun Kegiatan" >
                                    <option value=""></option>
                                    <option <?= $form_data->tahun_kegiatan == "2015" ? 'selected' :''; ?> value="2015">2015</option>
                                    <option <?= $form_data->tahun_kegiatan == "2016" ? 'selected' :''; ?> value="2016">2016</option>
                                    <option <?= $form_data->tahun_kegiatan == "2017" ? 'selected' :''; ?> value="2017">2017</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="file_1" class="col-sm-2 control-label">File 1 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_data_file_1_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_data_file_1_uuid" id="form_data_file_1_uuid" type="hidden" value="<?= set_value('form_data_file_1_uuid'); ?>">
                                <input class="data_file" name="form_data_file_1_name" id="form_data_file_1_name" type="hidden" value="<?= set_value('form_data_file_1_name', $form_data->file_1); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="file_2" class="col-sm-2 control-label">File 2 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_data_file_2_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_data_file_2_uuid" id="form_data_file_2_uuid" type="hidden" value="<?= set_value('form_data_file_2_uuid'); ?>">
                                <input class="data_file" name="form_data_file_2_name" id="form_data_file_2_name" type="hidden" value="<?= set_value('form_data_file_2_name', $form_data->file_2); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="file_3" class="col-sm-2 control-label">File 3 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_data_file_3_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_data_file_3_uuid" id="form_data_file_3_uuid" type="hidden" value="<?= set_value('form_data_file_3_uuid'); ?>">
                                <input class="data_file" name="form_data_file_3_name" id="form_data_file_3_name" type="hidden" value="<?= set_value('form_data_file_3_name', $form_data->file_3); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="file_4" class="col-sm-2 control-label">File 4 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_data_file_4_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_data_file_4_uuid" id="form_data_file_4_uuid" type="hidden" value="<?= set_value('form_data_file_4_uuid'); ?>">
                                <input class="data_file" name="form_data_file_4_name" id="form_data_file_4_name" type="hidden" value="<?= set_value('form_data_file_4_name', $form_data->file_4); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="file_5" class="col-sm-2 control-label">File 5 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="form_data_file_5_galery" ></div>
                                <input class="data_file data_file_uuid" name="form_data_file_5_uuid" id="form_data_file_5_uuid" type="hidden" value="<?= set_value('form_data_file_5_uuid'); ?>">
                                <input class="data_file" name="form_data_file_5_name" id="form_data_file_5_name" type="hidden" value="<?= set_value('form_data_file_5_name', $form_data->file_5); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="cancel (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_field_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/form_data';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_form_data = $('#form_form_data');
        var data_post = form_form_data.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_form_data.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id = $('#form_data_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
              var params = {};
       params[csrf] = token;

       $('#form_data_file_1_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/form_data/upload_file_1_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/form_data/delete_file_1_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/form_data/get_file_1_file/<?= $form_data->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_1_galery').fineUploader('getUuid', id);
                   $('#form_data_file_1_uuid').val(uuid);
                   $('#form_data_file_1_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_1_uuid').val();
                  $.get(BASE_URL + '/administrator/form_data/delete_file_1_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_1_uuid').val('');
                  $('#form_data_file_1_name').val('');
                }
              }
          }
      }); /*end file_1 galey*/
              var params = {};
       params[csrf] = token;

       $('#form_data_file_2_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/form_data/upload_file_2_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/form_data/delete_file_2_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/form_data/get_file_2_file/<?= $form_data->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_2_galery').fineUploader('getUuid', id);
                   $('#form_data_file_2_uuid').val(uuid);
                   $('#form_data_file_2_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_2_uuid').val();
                  $.get(BASE_URL + '/administrator/form_data/delete_file_2_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_2_uuid').val('');
                  $('#form_data_file_2_name').val('');
                }
              }
          }
      }); /*end file_2 galey*/
              var params = {};
       params[csrf] = token;

       $('#form_data_file_3_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/form_data/upload_file_3_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/form_data/delete_file_3_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/form_data/get_file_3_file/<?= $form_data->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_3_galery').fineUploader('getUuid', id);
                   $('#form_data_file_3_uuid').val(uuid);
                   $('#form_data_file_3_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_3_uuid').val();
                  $.get(BASE_URL + '/administrator/form_data/delete_file_3_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_3_uuid').val('');
                  $('#form_data_file_3_name').val('');
                }
              }
          }
      }); /*end file_3 galey*/
              var params = {};
       params[csrf] = token;

       $('#form_data_file_4_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/form_data/upload_file_4_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/form_data/delete_file_4_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/form_data/get_file_4_file/<?= $form_data->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_4_galery').fineUploader('getUuid', id);
                   $('#form_data_file_4_uuid').val(uuid);
                   $('#form_data_file_4_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_4_uuid').val();
                  $.get(BASE_URL + '/administrator/form_data/delete_file_4_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_4_uuid').val('');
                  $('#form_data_file_4_name').val('');
                }
              }
          }
      }); /*end file_4 galey*/
              var params = {};
       params[csrf] = token;

       $('#form_data_file_5_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/form_data/upload_file_5_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/form_data/delete_file_5_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/form_data/get_file_5_file/<?= $form_data->id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_5_galery').fineUploader('getUuid', id);
                   $('#form_data_file_5_uuid').val(uuid);
                   $('#form_data_file_5_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_5_uuid').val();
                  $.get(BASE_URL + '/administrator/form_data/delete_file_5_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_5_uuid').val('');
                  $('#form_data_file_5_name').val('');
                }
              }
          }
      }); /*end file_5 galey*/
           
    
    }); /*end doc ready*/
</script>