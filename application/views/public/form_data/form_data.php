    <script type="text/javascript" src="<?= BASE_ASSET; ?>js/ajax_daerah.js"></script>

<script src="<?= BASE_ASSET; ?>js/custom.js"></script>

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>

<?php $this->load->view('core_template/fine_upload'); ?>

<?= form_open('', [
    'name'    => 'form_form_data', 
    'class'   => 'form-horizontal form_form_data', 
    'id'      => 'form_form_data',
    'enctype' => 'multipart/form-data', 
    'method'  => 'POST'
]); ?>
 
<div class="form-group ">
    <label for="provinsi" class="col-sm-2 control-label">Provinsi 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select  class="form-control" name="provinsi" id="prop" data-placeholder="Select Provinsi"  onchange="ajaxkota(this.value)">
            <option value=""></option>
            <?php foreach (db_get_all_data('provinsi') as $row): ?>
            <option value="<?= $row->id_prov ?>"><?= $row->nama; ?></option>
            <?php endforeach; ?>  
        </select>
        <small class="info help-block">
        </small>
    </div>
</div>

 
<div class="form-group " id="kab_box">
    <label for="kota_kabupaten" class="col-sm-2 control-label">Kota / Kabupaten 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select name="kota_kabupaten" id="kota" class="form-control" onchange="ajaxkec(this.value)">
            <option value=""></option>
        </select>

        <small class="info help-block">
        </small>
    </div>
</div>

 
<div class="form-group " id="kec_box">
    <label for="kecamatan" class="col-sm-2 control-label">Kecamatan 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select  class="form-control" name="kecamatan" id="kec" data-placeholder="Select Kecamatan"  onchange="ajaxkel(this.value)">
            <option value=""></option>
          
        </select>

        <small class="info help-block">
        </small>
    </div>
</div>

 
<div class="form-group " id="kel_box">
    <label for="kelurahan_desa" class="col-sm-2 control-label">Kelurahan / Desa 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select  class="form-control" name="kelurahan_desa" id="kel" data-placeholder="Select Kelurahan / Desa"  >
            <option value=""></option>
        </select>
        <small class="info help-block">
        </small>
    </div>
</div>

 
<div class="form-group ">
    <label for="nama" class="col-sm-2 control-label">Nama 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="nama" id="nama" placeholder=""  >
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="nik" class="col-sm-2 control-label">NIK 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="nik" id="nik" placeholder=""  >
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="email" class="col-sm-2 control-label">Email 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="email" id="email" placeholder=""  >
        <small class="info help-block">
        <b>Format Email must</b> Valid Email.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <select  class="form-control chosen chosen-select" name="jenis_kelamin" id="jenis_kelamin" data-placeholder="Select Jenis Kelamin" >
            <option value=""></option>
            <option value="laki_laki">Laki - Laki</option>
            <option value="perempuan">Perempuan</option>
            </select>
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="tahun_kegiatan" class="col-sm-2 control-label">Tahun Kegiatan 
    </label>
    <div class="col-sm-8">
        <select  class="form-control chosen chosen-select" name="tahun_kegiatan" id="tahun_kegiatan" data-placeholder="Select Tahun Kegiatan" >
            <option value=""></option>
            <option value="2015">2015</option>
            <option value="2016">2016</option>
            <option value="2017">2017</option>
            </select>
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_1" class="col-sm-2 control-label">File 1 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_data_file_1_galery" ></div>
        <input class="data_file" name="form_data_file_1_uuid" id="form_data_file_1_uuid" type="hidden" >
        <input class="data_file" name="form_data_file_1_name" id="form_data_file_1_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_2" class="col-sm-2 control-label">File 2 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_data_file_2_galery" ></div>
        <input class="data_file" name="form_data_file_2_uuid" id="form_data_file_2_uuid" type="hidden" >
        <input class="data_file" name="form_data_file_2_name" id="form_data_file_2_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_3" class="col-sm-2 control-label">File 3 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_data_file_3_galery" ></div>
        <input class="data_file" name="form_data_file_3_uuid" id="form_data_file_3_uuid" type="hidden" >
        <input class="data_file" name="form_data_file_3_name" id="form_data_file_3_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_4" class="col-sm-2 control-label">File 4 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_data_file_4_galery" ></div>
        <input class="data_file" name="form_data_file_4_uuid" id="form_data_file_4_uuid" type="hidden" >
        <input class="data_file" name="form_data_file_4_name" id="form_data_file_4_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
    </div>
</div>
 
<div class="form-group ">
    <label for="file_5" class="col-sm-2 control-label">File 5 
    <i class="required">*</i>
    </label>
    <div class="col-sm-8">
        <div id="form_data_file_5_galery" ></div>
        <input class="data_file" name="form_data_file_5_uuid" id="form_data_file_5_uuid" type="hidden" >
        <input class="data_file" name="form_data_file_5_name" id="form_data_file_5_name" type="hidden" >
        <small class="info help-block">
        <b>Extension file must</b> JPG,JPEG,PNG,PDF.</small>
    </div>
</div>


<div class="row col-sm-12 message">
</div>
<div class="col-sm-2">
</div>
<div class="col-sm-8 padding-left-0">
    <button class="btn btn-flat btn-primary btn_save" id="btn_save" data-stype='stay'>
    Submit
    </button>
    <span class="loading loading-hide">
    <img src="http://localhost:80/project/cicool-daerah/asset//img/loading-spin-primary.svg"> 
    <i>Loading, Submitting data</i>
    </span>
</div>
</form></div>


<!-- Page script -->
<script>
    $(document).ready(function(){
          $('.form-preview').submit(function(){
        return false;
     });

     $('input[type="checkbox"].flat-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
     });


    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_form_data = $('#form_form_data');
        var data_post = form_form_data.serializeArray();
        var save_type = $(this).attr('data-stype');
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + 'form/form_data/submit',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id_file_1 = $('#form_data_file_1_galery').find('li').attr('qq-file-id');
            var id_file_2 = $('#form_data_file_2_galery').find('li').attr('qq-file-id');
            var id_file_3 = $('#form_data_file_3_galery').find('li').attr('qq-file-id');
            var id_file_4 = $('#form_data_file_4_galery').find('li').attr('qq-file-id');
            var id_file_5 = $('#form_data_file_5_galery').find('li').attr('qq-file-id');
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            if (typeof id_file_1 !== 'undefined') {
                    $('#form_data_file_1_galery').fineUploader('deleteFile', id_file_1);
                }
            if (typeof id_file_2 !== 'undefined') {
                    $('#form_data_file_2_galery').fineUploader('deleteFile', id_file_2);
                }
            if (typeof id_file_3 !== 'undefined') {
                    $('#form_data_file_3_galery').fineUploader('deleteFile', id_file_3);
                }
            if (typeof id_file_4 !== 'undefined') {
                    $('#form_data_file_4_galery').fineUploader('deleteFile', id_file_4);
                }
            if (typeof id_file_5 !== 'undefined') {
                    $('#form_data_file_5_galery').fineUploader('deleteFile', id_file_5);
                }
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 1000);
        });
    
        return false;
      }); /*end btn save*/


      
             
       var params = {};
       params[csrf] = token;

       $('#form_data_file_1_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_data/upload_file_1_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_data/delete_file_1_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_1_galery').fineUploader('getUuid', id);
                   $('#form_data_file_1_uuid').val(uuid);
                   $('#form_data_file_1_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_1_uuid').val();
                  $.get(BASE_URL + 'form/form_data/delete_file_1_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_1_uuid').val('');
                  $('#form_data_file_1_name').val('');
                }
              }
          }
      }); /*end file_1 galey*/
       var params = {};
       params[csrf] = token;

       $('#form_data_file_2_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_data/upload_file_2_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_data/delete_file_2_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_2_galery').fineUploader('getUuid', id);
                   $('#form_data_file_2_uuid').val(uuid);
                   $('#form_data_file_2_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_2_uuid').val();
                  $.get(BASE_URL + 'form/form_data/delete_file_2_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_2_uuid').val('');
                  $('#form_data_file_2_name').val('');
                }
              }
          }
      }); /*end file_2 galey*/
       var params = {};
       params[csrf] = token;

       $('#form_data_file_3_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_data/upload_file_3_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_data/delete_file_3_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_3_galery').fineUploader('getUuid', id);
                   $('#form_data_file_3_uuid').val(uuid);
                   $('#form_data_file_3_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_3_uuid').val();
                  $.get(BASE_URL + 'form/form_data/delete_file_3_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_3_uuid').val('');
                  $('#form_data_file_3_name').val('');
                }
              }
          }
      }); /*end file_3 galey*/
       var params = {};
       params[csrf] = token;

       $('#form_data_file_4_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_data/upload_file_4_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_data/delete_file_4_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_4_galery').fineUploader('getUuid', id);
                   $('#form_data_file_4_uuid').val(uuid);
                   $('#form_data_file_4_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_4_uuid').val();
                  $.get(BASE_URL + 'form/form_data/delete_file_4_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_4_uuid').val('');
                  $('#form_data_file_4_name').val('');
                }
              }
          }
      }); /*end file_4 galey*/
       var params = {};
       params[csrf] = token;

       $('#form_data_file_5_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + 'form/form_data/upload_file_5_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + 'form/form_data/delete_file_5_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg","jpeg","png","pdf"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#form_data_file_5_galery').fineUploader('getUuid', id);
                   $('#form_data_file_5_uuid').val(uuid);
                   $('#form_data_file_5_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#form_data_file_5_uuid').val();
                  $.get(BASE_URL + 'form/form_data/delete_file_5_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#form_data_file_5_uuid').val('');
                  $('#form_data_file_5_name').val('');
                }
              }
          }
      }); /*end file_5 galey*/
           
    }); /*end doc ready*/
</script>