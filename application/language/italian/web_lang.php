<?php
#cicool package
#languange Italian  code it
$lang['add_new_button'] = 'Aggiungere $1 Nuovo';
$lang['export_button'] = 'esportazione';
$lang['apply_button'] = 'applicare';
$lang['apply_bulk_action'] = 'Applicare un\'azione bulk';
$lang['all'] = 'tutto';
$lang['filter_button'] = 'filtro';
$lang['filter'] = 'filtro';
$lang['delete'] = 'chiaro';
$lang['filter_search'] = 'Ricerca raffinata';
$lang['reset_filter'] = 'Resettare la ricerca';
$lang['list_all'] = 'Elencare tutte $1';
$lang['data_is_not_avaiable'] = '$1 Scheda non disponibile';
$lang['view_button'] = 'guardare';
$lang['view_all_button'] = 'Vedi tutte $1';
$lang['update_button'] = 'Cambia $1';
$lang['remove_button'] = 'chiaro';
$lang['manage_button'] = 'gestire';
$lang['are_you_sure'] = 'Sei sicuro?';
$lang['data_to_be_deleted_can_not_be_restored'] = 'I dati che si elimina non può essere rimborsato!';
$lang['yes_delete_it'] = 'Sì, eliminarlo!';
$lang['no_cancel_plx'] = 'No, annullarlo!';
$lang['items'] = 'voce';
$lang['export'] = 'Le esportazioni $1';
$lang['please_choose_bulk_action_first'] = 'Si prega di selezionare la prima azione';
$lang['tool'] = '$1 attrezzo';
$lang['api_documentation'] = 'documentazione API';
$lang['api_key'] = 'API blocco';
$lang['send_button'] = 'presentare';
$lang['enter_request_url'] = 'Inserire le richieste URL';
$lang['status'] = 'stato';
$lang['time'] = 'tempo';
$lang['short_code_can_be_embed_in_page'] = 'Un codice corto può essere incorporato nella pagina';
$lang['short_code_can_be_embed_in_code'] = 'Un codice breve può essere incorporato nel codice';
$lang['new'] = '$1 Nuovo';
$lang['update'] = 'Aggiornare $1';
$lang['select'] = 'Seleziona $1';
$lang['save_button'] = 'Salva';
$lang['save_and_go_the_list_button'] = 'Salvare e andare alla lista';
$lang['cancel_button'] = 'nullo';
$lang['loading_field_data'] = 'Caricamento in corso, il caricamento dei dati';
$lang['create'] = 'fare';
$lang['read'] = 'leggere';
$lang['loading_saving_data'] = 'Caricamento, salvataggio dei dati';
$lang['table_is_being_for_generate'] = 'Tabella che verrà generato';
$lang['yes'] = 'sì';
$lang['no'] = 'Non lo è';
$lang['field'] = 'campo';
$lang['show_in'] = 'check in';
$lang['column'] = 'colonna';
$lang['add_form'] = 'forma supplementare';
$lang['add'] = 'addizionale';
$lang['update_form'] = 'modulo aggiornamento';
$lang['detail_page'] = 'Dettagli Previsioni';
$lang['input_type'] = 'tipo di feedback';
$lang['validation'] = 'Validazione';
$lang['detail'] = 'Dettagliata $1';
$lang['go_list_button'] = 'Vai alla lista di $1';
$lang['get_token'] = 'Ottieni un token';
$lang['go_list'] = 'Vai alla lista di $1';
$lang['page_settings'] = 'pagina delle impostazioni';
$lang['page_designer'] = 'Il progettista della pagina';
$lang['preview'] = 'anteprima';
$lang['blank'] = 'vuoto';
$lang['default'] = 'difetto';
$lang['frontend'] = 'prima pagina';
$lang['backend'] = 'cortile';
$lang['loading_generating_preview'] = 'Caricamento in corso, Creazione di anteprima';
$lang['add_block_element'] = 'Aggiungere Element Blocks';
$lang['all_elements'] = 'tutti gli elementi';
$lang['component'] = 'componente';
$lang['layout'] = 'disposizione';
$lang['element'] = 'elemento';
$lang['block'] = 'blocco';
$lang['detail_element'] = 'Elementi dettagliati';
$lang['clone_button'] = 'duplicare';
$lang['reset_button'] = 'ritorno';
$lang['apply_changes'] = 'l\'applicazione delle modifiche';
$lang['form_designer'] = 'Form Designer';
$lang['form_preview'] = 'anteprima modulo';
$lang['drag_form_here'] = 'Pull form che trovi qui';
$lang['loading_getting_data'] = 'Caricamento in corso, su Dati';
$lang['menu_type'] = 'tipo di Menu';
$lang['side_menu'] = 'menu laterale';
$lang['add_menu_type'] = 'Aggiungi tipo di Menu';
$lang['active'] = 'attivo';
$lang['inactive'] = 'inattivo';
$lang['check_all'] = 'Segna tutti';
$lang['loading_data'] = 'il caricamento dei dati';
$lang['undo_button'] = 'ritorno';
$lang['group'] = 'Gruppi $1';
$lang['do_not_be_fill_if_you_do_not_want_to_change_the_password'] = 'Non compilare se non si desidera cambiare la password';
$lang['web_setting'] = 'impostazioni del sito web';
$lang['setting'] = 'impostazioni del sito web';
$lang['system'] = 'sistema';
$lang['site_general'] = 'generale del sito';
$lang['site_name'] = 'nome del sito';
$lang['site_description'] = 'Descrizione sito';
$lang['site_description_help_block'] = 'La meta descrizione del sito.';
$lang['default_landing_page'] = 'iniziale predefinita';
$lang['default_landing_page_help_block'] = 'La pagina di destinazione predefinita, selezionare la pagina o di default tema.';
$lang['keyword'] = 'parola chiave';
$lang['default_theme'] = 'tema di default';
$lang['author'] = 'scrittore';
$lang['email'] = 'e-mail';
$lang['default_landing_page_select_from_page_or_default_theme'] = 'La pagina di destinazione predefinita, selezionare la pagina o di default tema.';
$lang['session_cokie_name'] = 'Sessione nome cookie';
$lang['session_cokie_name_help_block'] = 'Se si utilizza la crittografia di classe, è necessario impostare la chiave di crittografia.';
$lang['session_expiration'] = 'scadenza della sessione';
$lang['session_expiration_help_block'] = 'Il numero che si desidera in un momento di sessione di vita.';
$lang['session_time_to_update'] = 'Sessione tempo di aggiornare';
$lang['session_time_to_update_help_block'] = 'Quanto dura una rigenerazione ID di sessione.';
$lang['session_cookie_name'] = 'Nome del cookie di sessione';
$lang['csrf_token_name'] = 'CSRF nome del token';
$lang['csrf_cookie_name'] = 'nome del cookie CSRF';
$lang['csrf_expire'] = 'CSRF è scaduto';
$lang['csrf_expire_help_block'] = 'Numero di token deve scadere.';
$lang['permitted_uri_chars'] = 'Permette CARATTERI URI';
$lang['allowed_uri_chars_help_block'] = 'Effettivo valore configurabile è un gruppo di caratteri espressione regolare e verrà eseguito come: $1';
$lang['url_suffix'] = 'suffisso URL';
$lang['url_suffix_help_block'] = 'Questa opzione consente di aggiungere un suffisso a tutti gli URL generati. Recensioni
Esempio: $1';
$lang['global_xss_filtering'] = 'Filtraggio globale XSS';
$lang['global_xss_filtering_help_block'] = 'Determinare se il filtro XSS è sempre attivo quando i dati GET, POST o COOKIE trovato';
$lang['encryption_key'] = 'chiave di crittografia';
$lang['encryption_key_help_block'] = 'Se si utilizza la crittografia di classe, è necessario impostare la chiave di crittografia.';
$lang['sorry_you_do_not_have_permission_to_access'] = 'Siamo spiacenti, non è necessario il permesso di accedere.';
$lang['success_save_data_stay'] = 'I suoi dati sono stati salvati nel database. $1 o $2';
$lang['success_save_data_redirect'] = 'I suoi dati sono stati salvati nel database. $1';
$lang['data_not_change'] = 'I dati non sono cambiati.';
$lang['success_update_data_stay'] = 'I tuoi dati sono stati aggiornati con successo nel database. $1';
$lang['success_update_data_redirect'] = 'È stato aggiornato i dati nel database. $1';
$lang['has_been_deleted'] = '$1 eliminato correttamente.';
$lang['error_delete'] = '$1 non riuscito a cancellare.';
$lang['your_data_has_been_successfully_submitted'] = 'I suoi dati è stato inviato correttamente.';
$lang['sign_to_start_your_session'] = 'Il login per iniziare la sessione.';
$lang['error'] = 'Errore.';
$lang['i_forgot_my_password'] = 'Ho dimenticato la mia password.';
$lang['register_a_new_membership'] = 'Registra un nuovo abbonamento.';
$lang['sign_in'] = 'ingresso';
$lang['remember_me'] = 'Remind Me';
$lang['login'] = 'ingresso';
$lang['register'] = 'registrazione';
$lang['send_me_link_to_reset_password'] = 'Invia un link per reimpostare la password';
$lang['reset'] = 'reset';
$lang['human_challenge'] = 'sfida umana';
$lang['i_agree_to_the_terms'] = 'Sono d\'accordo con i termini';
$lang['i_already_a_new_membership'] = 'Sono diventato un nuovo membro';
$lang['get_your_system_cms_ecomerce_in_one_time'] = 'Ottenere il vostro sistema, CMS, e-commerce in un momento!';
$lang['cicool_web_builder_is_used__incridible'] = 'Cicool Web Builder viene utilizzato per creare web dinamica, fare API REST, forme dinamiche, CRUD dinamico e Page Builder incredibili!';
$lang['find_out_more'] = 'Per saperne di più';
$lang['we_have_got_what_you_need'] = 'Abbiamo ottenuto quello che <b> necessità </b>!';
$lang['cicool_made_to_facilitate__complex_application'] = 'Cicool fatto per aiutarvi nel vostro lavoro sul applicazione web, le caratteristiche principali del Builder API, Form Builder, CRUD Builder e si possono ridurre la Page Builder per costruire applicazioni complesse.';
$lang['buy_now'] = 'Acquista ora!';
$lang['common_features'] = 'La <b> Generale </b>';
$lang['make_rest_api_builder__generate_documentation'] = 'Creare API REST costruttore solo click, e ottenere automaticamente generato documentazione.';
$lang['you_can_make_dinamic_pages__element_avaiable'] = 'È possibile creare pagine dinamiche tramite trascinamento, più di $1 componenti e gli elementi sono disponibili.';
$lang['by_dragging_form_into_canvas__dinamic_form'] = 'Trascinando la tela per formare è possibile creare moduli dinamici.';
$lang['build_your_crud_in_one_click__input_type'] = 'CRUD ci si sveglia con un click, con il tipo di validazione dell\'input e il tipo di testo di tipo $$12.';
$lang['installation_just_one_sec__installation'] = 'L\'installazione richiede solo un secondo per l\'installazione guidata.';
$lang['awesome_admin_lte__abdullah_almsaeed'] = 'AdminLTE tema impressionante di $1.';
$lang['buy_now_and_you_wil_not_be_reversal'] = 'Acquista ora e <b> Non ve ne pentirete </b>.';
$lang['easy_installation'] = 'L\'installazione è facile.';
$lang['awesome_theme'] = 'tema impressionante.';
$lang['success'] = 'Successo.';
$lang['warning'] = 'Attenzione.';
$lang['is_writable'] = 'può essere scritta.';
$lang['wizzard_setup'] = 'Wizzard set-up';
$lang['wizzard'] = 'Wizzard';
$lang['permission_and_requirement'] = 'Permessi e requisiti';
$lang['directory_permission_and_requirements'] = 'Autorizzazioni e requisiti di directory';
$lang['directory_and_permission'] = 'Elenchi e Permessi';
$lang['server_requirements'] = 'Requisiti del server';
$lang['next'] = 'prossimo';
$lang['back'] = 'ritorno';
$lang['you_have_the_extension'] = 'Hai estensioni $1.';
$lang['you_have__or_greater_current_version'] = 'Hai PHP $1 (o superiore; Current Version: <b> $2 </b>)';
$lang['setup'] = 'configurazione';
$lang['configuration_setup'] = 'configurazione di setup';
$lang['system_configuration'] = 'configurazione del sistema';
$lang['database_setup'] = 'Database Setup';
$lang['database_and_site_configuration'] = 'Database Configuration & Site';
$lang['database_configuration'] = 'Configuration Database';
$lang['database_name'] = 'Nome database';
$lang['database_name_help_block'] = 'Il nome del database che si desidera chiamare.';
$lang['username'] = 'nome utente';
$lang['username_help_block'] = 'Il nome utente utilizzato per la connessione al database.';
$lang['password'] = 'password';
$lang['password_help_block'] = 'Password utilizzata per la connessione al database.';
$lang['database_host'] = 'Database Host';
$lang['database_host_help_block'] = 'Il nome host del server di database.';
$lang['site_configuration'] = 'Configurazione sito';
$lang['site_name_help_block'] = 'Web o applicazione nome che si desidera.';
$lang['site_email'] = 'sito e-mail';
$lang['site_email_help_block'] = 'E-mail può essere utilizzato per accedere al Web Administrator.';
$lang['site_password'] = 'password del sito';
$lang['site_password_help_block'] = 'Password verrà utilizzata per accedere al Web Administrator. Recensioni
lunghezza della password deve essere di 6 o più caratteri.';
$lang['there_are_some_files__write_mode_0666'] = 'Ci sono alcuni file che non possono essere letti dal sistema, assicurarsi che il file in modalità di scrittura 0666.';
$lang['there_are_several_server__this_problem'] = 'Ci sono alcuni sistemi server non sono soddisfatte, si prega di risolvere questo problema.';
$lang['select_language'] = 'selezionare Lingua';
$lang['setup_is_completed'] = 'Set-up è stato completato';
$lang['database_has_been_installed__the_page_administrator'] = 'Il database è stato installato aver completato il processo di configurazione del sito, la maggior parte premere il pulsante Fine per accedere alla pagina di amministratore';
$lang['finish'] = 'finito';
$lang['profile'] = 'profilo';
$lang['sign_out'] = 'uscita';
